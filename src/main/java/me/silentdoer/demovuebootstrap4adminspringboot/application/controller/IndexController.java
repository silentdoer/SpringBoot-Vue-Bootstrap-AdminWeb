package me.silentdoer.demovuebootstrap4adminspringboot.application.controller;

import lombok.extern.slf4j.Slf4j;
import me.silentdoer.demovuebootstrap4adminspringboot.application.mode.resp.SidebarHeader;
import me.silentdoer.demovuebootstrap4adminspringboot.application.mode.resp.SidebarItem;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author wlq
 * @version 1.0.0
 * @since 19-10-7 下午7:20
 */
@Slf4j
@RestController
@RequestMapping("/demovuebootstrap/api/application/index")
public class IndexController {

	@GetMapping("/sidebarHeader")
	public SidebarHeader sidebarHeader() {
		log.info("获取sidebarHeader数据");
		// 模拟从数据库里获取数据，所有页面的URL用相对路径，由前端去决定前缀是什么（防止域名变更之类的导致后端给出的URL无效）
		// 对于avatar和logo等图片的地址可以给绝对路径，因为它们一般不会放在项目内而且放在如OSS等地方（也可以放项目内，这时给相对路径）
		// 前端可以判断URL里是否有http,https等字段从而判断是否是绝对路径（或判断是否以/开头）
		return SidebarHeader.builder().brandName("Silentdoer")
				.linkUrl("/index.html")
				.logoUrl("http://localhost:8080/img/brand-white.svg").build();
	}

	@GetMapping("/sidebarItems")
	public List<SidebarItem> sidebarItems() {
		log.info("获取sidebarItems数据");
		// 模拟从数据库里获取数据，所有页面的URL用相对路径，由前端去决定前缀是什么（防止域名变更之类的导致后端给出的URL无效）
		// 对于avatar和logo等图片的地址可以给绝对路径，因为它们一般不会放在项目内而且放在如OSS等地方（也可以放项目内，这时给相对路径）
		// 前端可以判断URL里是否有http,https等字段从而判断是否是绝对路径（或判断是否以/开头）
		// TODO active由前端控制，这里的active作废，否则前端vue每次都会覆盖jquery设置的active导致一直不变化
		SidebarItem item1 = SidebarItem.builder().itemName("Dashboard")
				.iconType("chart").linkUrl("/index.html").build();

		SidebarItem item2 = SidebarItem.builder().itemName("Navigation")
				.linkUrl("/pages/navbar.html").iconType("menu")
				.badge("New").badgeType("success").build();

		List<SidebarItem> widgetsSubItems = new LinkedList<>();
		SidebarItem item3 = SidebarItem.builder().itemName("Widgets")
				.iconType("layers").badge("150+").badgeType("danger")
				.hasSub(true).subItems(widgetsSubItems).build();

		SidebarItem basic = SidebarItem.builder().itemName("Basic").linkUrl("/pages/widgets.html").build();
		SidebarItem statistic = SidebarItem.builder().itemName("Statistic").linkUrl("/pages/widget-statistic.html").build();
		SidebarItem data = SidebarItem.builder().itemName("Data").linkUrl("/pages/widget-data.html").build();
		SidebarItem chartWidget = SidebarItem.builder().itemName("Chart Widget").linkUrl("/pages/widget-chart.html").build();
		widgetsSubItems.addAll(Arrays.asList(basic, statistic, data, chartWidget));

		return new LinkedList<>(Arrays.asList(item1, item2, item3));
	}
}
