package me.silentdoer.demovuebootstrap4adminspringboot.application.mode.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author wlq
 * @version 1.0.0
 * @since 19-10-9 上午11:01
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SidebarItem {

	String linkUrl;

	String itemName;

	String iconType;

	String badge;

	String badgeType;

	boolean hasSub;

	List<SidebarItem> subItems;
}
