package me.silentdoer.demovuebootstrap4adminspringboot.application.mode.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wlq
 * @version 1.0.0
 * @since 19-10-9 上午11:00
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SidebarHeader {

	String linkUrl;

	String logoUrl;

	String brandName;
}
