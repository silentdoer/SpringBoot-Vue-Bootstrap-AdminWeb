package me.silentdoer.demovuebootstrap4adminspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoVueBootstrap4adminSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoVueBootstrap4adminSpringbootApplication.class, args);
	}

}
